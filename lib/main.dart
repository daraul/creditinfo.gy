import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      home: LoginPage(),
    ));
}

class Tabs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Text(
                'Consumer Information',
              ),
              Text(
                'Consumer Statements',
              ),
              Text(
                'Summary Information',
              ),
            ],
          ),
          title: Text('Creditinfo.GY'),
        ),
        body: TabBarView(
          children: [
            ConsumerInformationPage(first_name: 'Daraul Harris'),
            ConsumerStatementsPage(),
            SummaryInformationPage(),
          ],
        ),
      ),
    );
  }
}

class ConsumerInformationPage extends StatelessWidget {
  // This is the consumer information page

  ConsumerInformationPage({this.first_name});

  final String first_name;
  
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      padding: const EdgeInsets.all(32),
      children: [
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  'Creditinfo.GY',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[500],
                  )
                )
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  '${first_name}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  )
                )
              ),
              Text(
                '97 5th Street Cummings Lodge',
                style: TextStyle(
                  color: Colors.grey[500],
                )
              ),
            ]
          ),
        ),
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  'Equifax',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[500],
                  )
                )
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  '${first_name}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  )
                )
              ),
              Text(
                "353 Section 'A' Block 'X', Diamond Housing Scheme",
                style: TextStyle(
                  color: Colors.grey[500],
                )
              ),
            ]
          )
        )
      ]
    );
  }
}

class ConsumerStatementsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      child: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
            ),
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child: ListTile(
              title: Text('I refused to pay A Aly for the refridgerator they sold me because it stopped working days after I got it home, and they refused to honor the warranty and give me a replacement.'),
            )
          ),
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
            ),
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child: ListTile(
              title: Text('I did not pay Courts because the television they sold me stopped displaying a picture after only a week of use, and they refuse to give me a new one despite the warranty guaranteeing they would. They say that it is my fault that it broke, which is false.'),
            )
          ),
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
            ),
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child: ListTile(
              title: Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Augue eget arcu dictum varius duis at consectetur lorem donec.'),
            )
          ),
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
            ),
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child: ListTile(
              title: Text('Eu ultrices vitae auctor eu augue ut lectus arcu bibendum. Suspendisse potenti nullam ac tortor. Euismod in pellentesque massa placerat duis ultricies lacus sed.'),
            )
          ),
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
            ),
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child:  ListTile(
              title: Text('Ullamcorper a lacus vestibulum sed arcu non odio euismod lacinia. Aliquam etiam erat velit scelerisque in dictum non consectetur. Diam sit amet nisl suscipit adipiscing bibendum. Et pharetra pharetra massa massa.'),
            )
          ),
        ],
      )
    );
  }
}

class SummaryInformationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const containerPadding = 10.0;
    
    return Container(
      padding: const EdgeInsets.all(35),
      child: Table(
        border: TableBorder.all(
          color: Colors.grey[500],
          width: 3.0
        ),
        defaultVerticalAlignment: TableCellVerticalAlignment.top,
        children: [
          TableRow(
            children: [
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text(
                    'Real Estate Acconts',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    )
                  )
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text(
                    'Creditinfo.GY',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    )
                  )
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text(
                    'Equifax',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    )
                  )
                )
              )
            ]
          ),
          TableRow(
            children: [
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('Count ')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('2')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('1')
                )
              ),
            ]
          ),
          TableRow(
            children: [
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('Balance (\$) ')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('100223')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('3327')
                )
              ),
            ]
          ),
          TableRow(
            children: [
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('Payment (\$) ')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('332')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('6727')
                )
              ),
            ]
          ),
          TableRow(
            children: [
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('Current')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('1')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('1')
                )
              ),
            ]
          ),
          TableRow(
            children: [
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('Delinquent')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('0')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('1')
                )
              ),
            ]
          ),
          TableRow(
            children: [
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('Derogatory')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('0')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('0')
                )
              ),
            ]
          ),
          TableRow(
            children: [
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('Unknown')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('0')
                )
              ),
              TableCell(
                child: Container(
                  padding: const EdgeInsets.all(containerPadding),
                  child: Text('0')
                )
              ),
            ]
          ),
        ]
      )
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() {
    return LoginPageState();
  }
}

// Define a corresponding State class.
// This class holds data related to the form.
class LoginPageState extends State<LoginPage> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Container(
      color: Colors.grey[200],
      child: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(12),
              child: Text(
                'Welcome to Creditinfo.GY',
                style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                  color: Colors.blue,
                  decoration: TextDecoration.none,
                ),
              )
            ),
            Container(
              padding: const EdgeInsets.all(12),
              child: Text(
                "Credit is important, and so is understanding your credit. Guyanese are still new to this concept, therefore key to helping the populace understand their credit is helping them to see their credit report. Creditinfo Guyana affords anyone the ability to view a credit report via email. In short, give them your email address, and some identifying information, and once a year, you're allowed to view your credit report for free. I thought the next step from here would be to let anyone view their credit report live, anytime, anywhere, from any of their internet connected devices.",
                textAlign: TextAlign.justify,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  decoration: TextDecoration.none,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(12),
              child: Text(
                "This application is built with a cross-platform framework, allowing swift deployment to iOS, Android, and the web. It provides users with an intuitive, and powerful portal through which they can inspect, and therefore understand their credit, what impacts it, and how. This can help Guyanese to make better financial decisions, bettering the nation as a whole.",
                textAlign: TextAlign.justify,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  decoration: TextDecoration.none,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(12),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Tabs()),
                  );
                },
                child: Container(
                  padding: const EdgeInsets.all(15),
                  child: Text(
                    "Take a look",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 36.0,
                      fontWeight: FontWeight.normal,
                      decoration: TextDecoration.none,
                    ),
                  )
                )
              )
            )
          ]
        )
      )
    );
  }
}
